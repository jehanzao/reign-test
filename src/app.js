import express from 'express';
import morgan from 'morgan';
import cors from 'cors';
import articleRoutes from './routes/article.routes'
import { urlencoded } from 'express';

const app = express();

app.use(morgan('dev'));
app.use(express.json());
app.use(cors());
app.use(urlencoded({extended:false}))


app.use('/api/v1/articles', articleRoutes);

export default app;