import axios from'axios';
import cron from 'node-cron';
import Article from '../models/article.model'
import Deleted from '../models/deleted.model'

export const autoGetArticle = (req, res) => {
    const getArticle = () => {
        console.log('running every hour');
        axios.get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs')
        .then(res=> {
            var articles = res.data.hits;

            articles.forEach(async article => {
                const isArticle = await Article.findOne({title: article.story_title})
                const isDeleted = await Deleted.findOne({title: article.story_title})
                if (!isArticle && !isDeleted) {
                    const newArticle = new Article({
                        title: article.story_title,
                        url: article.story_url,
                        author: article.author,
                        points: article.points,
                        story_text: article.story_text,
                        comment_text: article.comment_text,
                        num_comments: article.num_comments,
                        story_id: article.story_id,
                        parent_id: article.parent_id,
                        tags: article._tags,
                        created_at: article.created_at,
                    });

                    const savedArticles = await newArticle.save()
                    console.log('new article saved')
                } else {
                    console.log('article already in DB');
                }
            });
        })
        .catch(error => console.log(error))
        .finally(()=> {
            console.log('review of articles complete');
        })
    }
    getArticle();

cron.schedule('* 1 * * *', () => {
    getArticle();
});
    
}
