import mongoose from 'mongoose';
import {env} from './env'
// const mongoose = require('mongoose');

const  MONGODB_URI  = `mongodb+srv://${env.DBUSERNAME}:${env.DBPASSWORD}@cluster0.cbuth.mongodb.net/${env.DBNAME}?retryWrites=true&w=majority`

mongoose.connect(MONGODB_URI, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
})
    .then(() => console.log('database is connected'))
    .catch(err => console.log(err));