import app from './app';
import database from './database'
import {autoGetArticle} from './controllers/getArticles.controller'


const server = app.listen(5000, () => {
    autoGetArticle()
    console.log("conected to port", 5000);
});

export default server;