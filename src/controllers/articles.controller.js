import Article from '../models/article.model'
import Deleted from '../models/deleted.model'

export const getArticles = async (req, res) => {
    try {
        var page = parseInt(req.params.page)    
        var pageSkipped = 0;

        page < 1 ? pageSkipped = 0: pageSkipped = (page-1)*5;

        const articles = await Article.find().skip(pageSkipped).limit(5);
        
        if (articles.length == 0) {
            return res.status(404).json({message: 'There is no more pages to search'})
        }
        console.log(articles);

        return res.status(200).json({ data: articles, message: 'articles in DB'})
    } catch (error) {
        console.log(error);
        return res.status(500).json({message: 'unexpected error'})
    }
    
}

export const getFilterArticles = async (req, res) => {
    try {
        const {tags, author, title} = req.query
        var page = parseInt(req.params.page)    
        var pageSkipped = 0;

        page < 1 ? pageSkipped = 0: pageSkipped = (page-1)*5;

        if (author) {
            const articles = await Article.find({author: author}).skip(pageSkipped).limit(5)
            return res.status(200).json({ data: articles, message: 'articles in DB'})
    
        } else if (title){
            const articles = await Article.find({title: title}).skip(pageSkipped).limit(5)
            return res.status(200).json({ data: articles, message: 'articles in DB'})
    
        } else if (tags){
            const articles = await Article.find({tags: {$all: tags.split(',')}}).skip(pageSkipped).limit(5)
            return res.status(200).json({ data: articles, message: 'articles in DB'})
    
        } else {
            return res.status(404).json({message: 'there is nothing like you are looking for :/'})
        }
    } catch (error) {
        console.log(error);
        return res.status(500).json({message: 'unexpected error'})
    }  
}

export const deleteArticle = async (req, res) => {
    try {
        const deleted = await Article.findByIdAndDelete(req.params.id)
        const newDeleted = new Deleted({
            title: deleted.title
        })
        newDeleted.save()
        return res.status(200).json({message: 'article erased correctly'})
    } catch (error) {
        console.log(error);
        return res.status(500).json({message: 'unexpected error'})
    }
}