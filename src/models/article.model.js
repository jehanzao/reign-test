import { Schema , model } from "mongoose";

const articleSchema = new Schema({
    title: String,
    url: String,
    author: String,
    points: Number,
    story_text: String,
    comment_text: String,
    num_comments: Number,
    story_id: Number,
    parent_id: Number,
    tags: [String],
    created_at: Date,
})

export default model('Article', articleSchema)