import app from '../src/app';
import request from 'supertest';
import mongoose from 'mongoose';
import server from '../src/index'

jest.setTimeout(1000000)

describe('Article controllers', ()=> {

    test('Find 5 articles', async() => { 
        const response = await request(app).get('/api/v1/articles/page=1')
        .expect(200)
        .expect('Content-Type', /json/)
    })

    test('There is no more articles', async() => { 
        const response = await request(app).get('/api/v1/articles/page=30')
        .expect(404)
        .expect('Content-Type', /json/)
    })

    test('Find filter articles by tags', async() => { 
        const response = await request(app).get('/api/v1/articles/filter/page=1?tags=comment')
        .expect(200)
        .expect('Content-Type', /json/)
    })

    test('Find filter articles by author', async() => {
        const response = await request(app).get('/api/v1/articles/filter/page=1?author=Ygg2')
        .expect(200)
        .expect('Content-Type', /json/)
    })

    test('Find filter articles by title', async() => {
        const response = await request(app).get('/api/v1/articles/filter/page=1?title=Statement on 4 Years of GDPR')
        .expect(200)
        .expect('Content-Type', /json/)
    })
})
