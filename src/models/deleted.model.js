import { Schema, model } from "mongoose";

const deletedSchema = new Schema({
    title: String
})

export default model('Deleted', deletedSchema)