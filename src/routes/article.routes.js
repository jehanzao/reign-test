import * as articleCtrl from '../controllers/articles.controller';
import {Router} from 'express'

const router = Router()

router.get('/page=:page', articleCtrl.getArticles)

router.get('/filter/page=:page', articleCtrl.getFilterArticles)

router.delete('/:id', articleCtrl.deleteArticle)


export default router